"""restaurantAPI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from restaurantApp import views
from restaurantApp.views import productViews, tableViews
from restaurantApp.views import paymentViews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.userViews.UserCreateView.as_view()),
    path('user/<int:pk>/', views.userViews.UserDetailView.as_view()),

    path('products/all/', productViews.ProductList.as_view()),
    path('product/<slug:product_slug>/', productViews.ProductDetail.as_view()),
    path('category/<slug:category_slug>/', productViews.CategoryDetail.as_view()),

    path('tables/all/', tableViews.TableList.as_view()),
    path('table/<int:pk>/', tableViews.TableDetail.as_view()),

    path('order/', views.orderViews.CreateOrder.as_view()),
    path('orders/all/', views.orderViews.OrderList.as_view()),
    path('order/<int:pk>/', views.orderViews.OrderDetail.as_view()),

    path('payment/', paymentViews.CreatePayment.as_view()),
    path('payments/all/', paymentViews.PaymentList.as_view()),
    path('payment/<int:pk>/', paymentViews.PaymentDetail.as_view())
]
