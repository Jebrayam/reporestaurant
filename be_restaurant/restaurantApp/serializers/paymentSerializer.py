from rest_framework import serializers
from restaurantApp.models.payment import Payment
from restaurantApp.models.product import Product
from restaurantApp.serializers.orderSerializer import OrderSerializer

class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields =["id", "date_time", "user", "order", "payment_method", "total"]

    def create(self, validated_data):           
        order_serializer = OrderSerializer(validated_data['order'])

        total = 0
        for ordered in order_serializer.data['ordered_products']:
            quantity = ordered['quantity']
            product_price = Product.objects.get(
                                pk=order_serializer.data['ordered_products'][0]['product']).price

            total += quantity*product_price
        
        validated_data['total'] = total
        payment_instance = Payment.objects.create(**validated_data)     

        return payment_instance