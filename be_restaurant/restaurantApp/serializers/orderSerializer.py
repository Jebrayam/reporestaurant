from rest_framework import serializers

from restaurantApp.models.order import Order, OrderProduct

class OrderProductSerializer(serializers.ModelSerializer):    
    class Meta:
        model = OrderProduct
        fields = ["id", "order", "product", "quantity"]

class OrderSerializer(serializers.ModelSerializer):
    ordered_products = OrderProductSerializer(many=True)

    class Meta:
        model = Order
        fields = ["id", "table", "date_time", "user", "ordered_products"]

    def create(self, validate_data):
        products_data = validate_data.pop('ordered_products')
        order_instance = Order.objects.create(**validate_data)

        for product in products_data:
            OrderProduct.objects.create(order_id=order_instance.id, **product)
        
        return order_instance

