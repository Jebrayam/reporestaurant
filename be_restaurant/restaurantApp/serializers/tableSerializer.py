from rest_framework import serializers
from restaurantApp.models.table import Table

class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = ["id", "n_chairs", "available", "reserved_by"]