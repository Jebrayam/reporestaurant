from rest_framework import serializers

from restaurantApp.models.category import Category
from restaurantApp.models.product import Product

from .productSerializer import ProductSerializer

class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta: 
        model = Category
        fields = ['id', 'name', 'products', 'get_absolute_url']