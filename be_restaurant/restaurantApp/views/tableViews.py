from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from rest_framework import status

from restaurantApp.models.table import Table
from restaurantApp.serializers.tableSerializer import TableSerializer

class TableList(APIView):
    def get(self, request, format=None):
        tables = Table.objects.all()
        serializer = TableSerializer(tables, many=True)

        return Response(serializer.data)

class TableDetail(APIView):
    def get_object(self, pk):
        try:
            return Table.objects.get(id=pk)
        except Table.DoesNotExist:
            raise Http404
    
    def get(self, request, pk, format=None):
        table = self.get_object(pk)
        serializer = TableSerializer(table)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        table = self.get_object(pk)
        serializer = TableSerializer(table, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)