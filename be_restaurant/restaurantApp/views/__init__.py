from .userViews import UserCreateView, UserDetailView
from .productViews import ProductList, CategoryDetail, ProductDetail
from .tableViews import TableList, TableDetail
from .orderViews import CreateOrder, OrderDetail, OrderList
from .paymentViews import PaymentList, PaymentDetail, CreatePayment