from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(blank=True, null=True)

    class Meta:
        ordering = ('name',)

    def get_absolute_url(self):
        return f'/{self.slug}/'