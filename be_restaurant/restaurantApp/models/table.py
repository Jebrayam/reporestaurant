from django.db import models

class Table(models.Model):
    id = models.BigAutoField(primary_key=True)
    n_chairs = models.IntegerField(blank=True, null=True)
    available = models.BooleanField(default=True)
    reserved_by = models.CharField(max_length=255 ,blank=True, null=True)