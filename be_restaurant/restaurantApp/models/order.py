from django.db import models
from restaurantApp.models.product import Product

from restaurantApp.models.table import Table
from restaurantApp.models.user import User

class Order(models.Model):
    id = models.BigAutoField(primary_key=True)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class OrderProduct(models.Model):
    order = models.ForeignKey(Order, related_name="ordered_products", 
                                on_delete=models.CASCADE, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)