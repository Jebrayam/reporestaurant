from django.db import models
from .category import Category

class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    date_added = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(blank=True, null=True)

    def get_absolute_url(self):
        return f'/{self.category.slug}/{self.slug}/'