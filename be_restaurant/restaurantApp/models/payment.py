from django.db import models
from restaurantApp.models.user import User
from restaurantApp.models.order import Order

class Payment(models.Model):
    id = models.BigAutoField(primary_key=True)
    date_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    payment_method = models.CharField(max_length=10)
    total = models.DecimalField(max_digits=8, decimal_places=2, blank=True, 
                                null=True)